% Created 2022-02-14 Mon 21:04
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\date{}
\title{DevSecOps}
\hypersetup{
 pdfauthor={},
 pdftitle={DevSecOps},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.5.2)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Security}
\label{sec:org7e24571}
Security is a broad subject, we're only going to scrape the surface today, as
well as trying to establish some ground rules.
\section*{Security is \textbf{everything}}
\label{sec:org000eff9}
It doesn't matter how good your software is, if it's insecure it won't be used.

Trust is hard to re-build once lost (if you accidentally leak all your
customers' data, they won't be your customers for long
\section*{Continuous Threats}
\label{sec:org2a7a4af}
Your attacker isn't necessarily a physical person sitting in from of a screen,
as we have CI/CD, there are also ways to implement continuous exploitation. If
your software is exposed to the Internet, consider it under active attack.
\section*{A few golden rules}
\label{sec:org9c751af}
Here come an incomplete list over things that are good to keep in mind when
implementing and deploying IT systems.
\subsection*{Don't roll your own crypto}
\label{sec:org80afbb1}
Don't implement your own cryptography. Rely on mainstream, well-established
solutions. Free and Open Source Software (FOSS) here is a big plus, as the
implementation can be publicly assessed.

Modern cryptography relies heavily on very complex math, and getting it right is
almost impossible to do for one single person.
\subsection*{Don't roll your own auth}
\label{sec:orgda1f638}
By extension of \textbf{\textbf{don't roll your own crypto}}, don't roll your own
authentication/authorization.

Some form of authentication and authorization are present in all projects
(except trivial/personal projects maybe). It's tedious to implement (imho), easy
to get wrong and can have disastrous consequences if done wrong. Rely on
well-known and well-tested solutions where possible.
\subsection*{Avoid security by obscurity}
\label{sec:orga1aaf3b}
"Security by obscurity" means considering something secure because the exact
details of its protection are kept secret. This is not a good strategy, as the
secret process can be reverse-engineered.
\subsection*{Encrypt communications}
\label{sec:org5ceb512}
By default TCP/IP (OSI model) isn't encrypted per-se. This means that all
communications can be listened by third parties (i.e. ISPs) and even
manipulated. You need to encrypt all channels of communication to render them
secure.

An example is avoiding to use or expose \texttt{telnet}, which is an older unencrypted
protocol, opting instead for \texttt{ssh}, which is encrypted.
\subsubsection*{SSL}
\label{sec:orgb4bb8f1}
SSL (secure socket layer) was a standard of encryption to secure channels, such
as HTTP (rendering it HTTPS) or FTP (FTPS).

SSL is deprecated in favour of TLS, which accomplishes the same thing with more
modern encryption.
\subsubsection*{TLS}
\label{sec:org01f6663}
Transport Layer Security encrypts communication channels.
\subsubsection*{TLS 101}
\label{sec:org76edcc9}
TLS relies on the concept of trust to secure communications.

For example an unencrypted HTTP service can be turned into an encrypted HTTPS
service.
\subsubsection*{Trust how?}
\label{sec:org50d5b5c}
TLS uses trust which is based on digital certificates.

A digital certificate is a digitally signed piece of information stating you are
who you claim to be. As long as you trust whoever signed the certificate, you
can trust the certificate.
\subsubsection*{Trust who?}
\label{sec:orge53234c}
But how do you know when to trust signers of digital certificates? Your OS, and
your browser, come with a list of trusted signers (CA, or Certificate
Authorities) that are trusted by default. You can add your own, but be sure to
know what you're doing.
\subsubsection*{Self signed certificates}
\label{sec:org8487017}
TLS can even be rolled with so-called self-signed certificates. That's a
certificate that signed itself (broad oversimplification) and can be useful to
secure local or temporary services, but often you need to manually accept the
certificate.
\subsubsection*{Ultimately, you're trusting someone}
\label{sec:org03a03ca}
Ultimately, TLS relies on trusting the CAs to not have security vulnerabilities
in their system, and browsers and OSs to make the right choice on which CAs to
trust.

If a trusted CA get compromised we are left vulnerable to MitM
(man-in-the-middle) and other attacks.
\subsubsection*{Let's Encrypt}
\label{sec:org71e9d8a}
CAs-signed certificates can be quite expensive. \href{https://letsencrypt.org}{Let's Encrypt} is a nonprofit CA
providing free TLS. You only need to demonstrate you control a certain domain
name and can obtain TLS certificates for free.

They also provide ways to automate certificate renewal.
\subsection*{Limit access}
\label{sec:org1fc4695}
Make sure to always close down systems as much as possible. Firewalls are an
indispensable example of limiting access at the network level.

Other examples:
\begin{itemize}
\item don't give out full-blown \texttt{sudo} rights, but limit which commands can be run
through sudo
\item whitelist which users are allowed to ssh into a system
\end{itemize}
\subsection*{Actively respond to attacks}
\label{sec:orgfe9d501}
There are tools that allow you to gain precious intel on ongoing attacks, such
as IDS (Intrusion Detection Systems) and to automatically respond to them, such
as IPS (Intrusion Prevention Systems).

Fail2ban is another example of actively responding to threats by dynamically
creating iptables rules to block users trying to break authentication.
\subsection*{Beware of SQL injections}
\label{sec:org8cd7df6}
\begin{center}
\includegraphics[width=.9\linewidth]{./exploits_of_a_mom.png}
\end{center}
\subsection*{Monitor your systems}
\label{sec:orgb3fb2d3}
Monitoring systems help with more than just keeping an eye on how much resources
your system is using.

A good example of security related monitoring rules is actively monitor
\texttt{/etc/passwd} for changes, and alerting when the file changes.
\subsection*{An uphill battle}
\label{sec:org82ddb92}
Securing systems is an ever-ongoing process (i.e. keeping systems up to date
with the latest security patches) and can sometimes feel as an uphill struggle\ldots{}

\ldots{}that's because it is! You need to keep your system secure 100\% of the time,
but an attacker needs only 1 successful exploitation attempt.

Don't let this fact frustrate you. Keep your systems safe as best you can,
knowing it will always be an ongoing process, and not a single action.
\subsection*{The weakest link in the chain}
\label{sec:org779a600}
ARE ALWAYS HUMANS.

It's very hard to break modern encryption (as far as we know), it might be
easier to call the reception, pretend to be a new hire and get access to the
system via human intervention. Social engineering is an art in and of itself,
and educating the users against it isn't always an easy task.
\section*{Security by Design}
\label{sec:orga285563}
Security shouldn't be an extra layer you add on top when everything else is
done, but an integral part of the system since the design. This way it will be
easier to keep the system secure over time.
\section*{DevSecOps}
\label{sec:orgbe6ee51}
DevSecOps is an addition to DevOps to allow for security practices to be a part
of the DevOps workflow. Every team is empowered with implementing the correct
security controls in their system.

Code can be be tested for security issues either by testing the internals of the
system (white-box-testing) or by testing its functionality (black-box-testing).
\section*{Workshop}
\label{sec:org354c785}
We'll now setup an HTTP service, and later secure it with TLS with the help of a
reverse proxy.
\end{document}